<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Auth::routes();

Route::resource('countries', 'CountriesController', ['except' => ['show']]);
Route::resource('states', 'StatesController', ['except' => ['show']]);
Route::resource('counties', 'CountiesController', ['except' => ['show']]);
Route::resource('taxes', 'TaxesController', ['except' => ['show']]);
Route::get('/taxReports/stateTotalCollected', 'TaxReportsController@stateTotalCollected');
Route::get('/taxReports/stateAverageCollected', 'TaxReportsController@stateAverageCollected');
Route::get('/taxReports/stateAverageTaxRate', 'TaxReportsController@stateAverageTaxRate');
Route::get('/taxReports/countryAverageTaxRate', 'TaxReportsController@countryAverageTaxRate');
Route::get('/taxReports/countryTotalTaxes', 'TaxReportsController@countryTotalTaxes');
