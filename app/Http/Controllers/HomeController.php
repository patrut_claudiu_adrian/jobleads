<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index(){
        $title = 'Welcome to Jobleads!';

        if (!auth()->guest()){
            return redirect('/dashboard');
        }

        return view('homepage.index')->with('title', $title);
    }
}
