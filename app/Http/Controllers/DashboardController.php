<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $userId = auth()->user()->id;
        $User   = User::find($userId);

        $Countries = DB::table('countries')
            ->select(DB::raw('COUNT(*) as total_countries'))
            ->first();

        $States = DB::table('states')
            ->select(DB::raw('COUNT(*) as total_states'))
            ->first();

        $Counties = DB::table('counties')
            ->select(DB::raw('COUNT(*) as total_counties'))
            ->first();

        $TaxesNumber = DB::table('taxes')
            ->select(DB::raw('COUNT(*) as total_taxes'))
            ->first();

        $TaxesAmount = DB::table('taxes')
            ->select(DB::raw('SUM(tax_amount) as taxes_amount'))
            ->first();

        $TaxesAverage = DB::table('taxes')
            ->select(DB::raw('ROUND(AVG(tax_amount), 2) as taxes_average'))
            ->first();

        return view('dashboard')
            ->with('User', $User)
            ->with('Countries', $Countries)
            ->with('States', $States)
            ->with('Counties', $Counties)
            ->with('TaxesNumber', $TaxesNumber)
            ->with('TaxesAmount', $TaxesAmount)
            ->with('TaxesAverage', $TaxesAverage);
    }
}
