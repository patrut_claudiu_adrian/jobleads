<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class TaxReportsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stateTotalCollected(){

        $Taxes = DB::table('taxes')
            ->join('counties', 'counties.id', '=', 'taxes.county_id')
            ->join('states', 'states.id', '=', 'counties.state_id')
            ->join('countries', 'countries.id', '=', 'states.country_id')
            ->select('countries.country_name', 'states.state_name', DB::raw('SUM(taxes.tax_amount) as tax_amount'))
            ->groupBy('countries.country_name', 'states.state_name')
            ->orderBy('countries.country_name')
            ->orderBy('states.state_name')
            ->paginate(10);

        return view('taxReports.stateTotalCollected')->with('Taxes', $Taxes);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stateAverageCollected(){

        $Taxes = DB::table('taxes')
            ->join('counties', 'counties.id', '=', 'taxes.county_id')
            ->join('states', 'states.id', '=', 'counties.state_id')
            ->join('countries', 'countries.id', '=', 'states.country_id')
            ->select('countries.country_name', 'states.state_name', DB::raw('ROUND(AVG(taxes.tax_amount), 2) as tax_amount'))
            ->groupBy('countries.country_name', 'states.state_name')
            ->orderBy('countries.country_name')
            ->orderBy('states.state_name')
            ->paginate(10);

        return view('taxReports.stateAverageCollected')->with('Taxes', $Taxes);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stateAverageTaxRate(){

        $States = DB::table('counties')
            ->join('states', 'states.id', '=', 'counties.state_id')
            ->join('countries', 'countries.id', '=', 'states.country_id')
            ->select('countries.country_name', 'states.state_name', DB::raw('ROUND(AVG(counties.tax_rate), 2) as tax_rate'))
            ->groupBy('countries.country_name', 'states.state_name')
            ->orderBy('countries.country_name')
            ->orderBy('states.state_name')
            ->paginate(10);

        return view('taxReports.stateAverageTaxRate')->with('States', $States);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function countryAverageTaxRate(){

        $Countries = DB::table('counties')
            ->join('states', 'states.id', '=', 'counties.state_id')
            ->join('countries', 'countries.id', '=', 'states.country_id')
            ->select('countries.country_name', DB::raw('ROUND(AVG(counties.tax_rate), 2) as tax_rate'))
            ->groupBy('countries.country_name')
            ->orderBy('countries.country_name')
            ->paginate(10);

        return view('taxReports.countryAverageTaxRate')->with('Countries', $Countries);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function countryTotalTaxes(){

        $Taxes = DB::table('taxes')
            ->join('counties', 'counties.id', '=', 'taxes.county_id')
            ->join('states', 'states.id', '=', 'counties.state_id')
            ->join('countries', 'countries.id', '=', 'states.country_id')
            ->select('countries.country_name', DB::raw('SUM(taxes.tax_amount) as tax_amount'))
            ->groupBy('countries.country_name')
            ->orderBy('countries.country_name')
            ->paginate(10);

        return view('taxReports.countryTotalTaxes')->with('Taxes', $Taxes);

    }
}
