<?php

namespace App\Http\Controllers;

use App\Country;
use App\County;
use App\State;
use Illuminate\Http\Request;

class StatesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $States = State::orderBy('id')->paginate(10);

        return view('states.index')->with('States', $States);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $Countries     = Country::orderBy('country_name')->get();
        $countriesList = ['' => 'Select country'];

        foreach ($Countries as $Country) {
            $countriesList[$Country->id] = $Country->country_name;
        }

        return view('states.create')->with('countriesList', $countriesList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'state_name' => 'required|unique:states,state_name,NULL,id,country_id,'.$request->input('country_id'),
            'country_id' => 'required|regex:/^[0-9]*$/',
        ]);

        $Country = Country::find($request->input('country_id'));

        if ($Country === null) {
            return redirect('/states')->with('error', 'Invalid country ID');
        }

        $State             = new State();
        $State->state_name = $request->input('state_name');
        $State->country_id = $request->input('country_id');
        $State->save();

        return redirect('/states')->with('success', 'State added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $State = State::find($id);

        if ($State === null) {
            return redirect('/states')->with('error', 'Invalid state ID');
        }

        $Countries     = Country::orderBy('country_name')->get();
        $countriesList = ['' => 'Select country'];

        foreach ($Countries as $Country) {
            $countriesList[$Country->id] = $Country->country_name;
        }

        return view('states.edit')->with('State', $State)->with('countriesList', $countriesList);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'state_name' => 'required|unique:states,state_name,'.$id.',id,country_id,'.$request->input('country_id'),
            'country_id' => 'required|regex:/^[0-9]*$/',
        ]);

        $State = State::find($id);

        if ($State === null) {
            return redirect('/states')->with('error', 'Invalid state ID');
        }

        $Country = Country::find($request->input('country_id'));

        if ($Country === null) {
            return redirect('/states')->with('error', 'Invalid country ID');
        }

        $State->state_name = $request->input('state_name');
        $State->country_id = $request->input('country_id');
        $State->save();

        return redirect('/states')->with('success', 'State updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $State = State::find($id);

        if ($State === null) {
            return redirect('/states')->with('error', 'Invalid state ID');
        }

        $Counties = County::where('state_id', $id)->limit(1)->get();

        if (count($Counties) > 0) {
            return redirect('/states')->with('error', 'Please remove all counties from this state');
        }

        $State->delete();

        return redirect('/states')->with('success', 'State deleted');
    }
}
