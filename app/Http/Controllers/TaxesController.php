<?php

namespace App\Http\Controllers;

use App\Country;
use App\County;
use App\State;
use App\Tax;
use Illuminate\Http\Request;

class TaxesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $Taxes = Tax::orderBy('id')->paginate(10);

        return view('taxes.index')->with('Taxes', $Taxes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $Countries     = Country::orderBy('country_name')->get();
        $countiesList  = ['' => 'Select county'];
        $countiesTaxes = [];

        foreach ($Countries as $Country) {

            $States = State::orderBy('state_name')->where('country_id', $Country->id)->get();

            if (count($States) > 0) {

                foreach ($States as $State) {

                    $Counties = County::orderBy('county_name')->where('state_id', $State->id)->get();

                    if (count($Counties) > 0) {

                        $optGroup = $Country->country_name.' -> '.$State->state_name;

                        foreach ($Counties as $County) {
                            $countiesList[$optGroup][$County->id] = $County->county_name;
                            $countiesTaxes[$County->id]           = $County->tax_rate;
                        }
                    }
                }

            }
        }

        return view('taxes.create')->with('countiesList', $countiesList)->with('countiesTaxes', json_encode($countiesTaxes));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'income_value' => ['required', 'numeric', 'min:0', 'not_in:0', 'regex:/^(\d{1,2}|\d{3,4}|\d{5,6})(\.\d{1,2})?$/'],
            'county_id'    => 'required|regex:/^[0-9]*$/',
        ]);

        $County = County::find($request->input('county_id'));

        if ($County === null) {
            return redirect('/taxes')->with('error', 'Invalid county ID');
        }

        $Tax               = new Tax();
        $Tax->income_value = $request->input('income_value');
        $Tax->tax_rate     = $County->tax_rate;
        $Tax->tax_amount   = round($request->input('income_value') * $County->tax_rate / 100, 2);
        $Tax->county_id    = $request->input('county_id');
        $Tax->save();

        return redirect('/taxes')->with('success', 'Tax collected');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $Tax = Tax::find($id);

        if ($Tax === null) {
            return redirect('/taxes')->with('error', 'Invalid tax ID');
        }

        $Countries     = Country::orderBy('country_name')->get();
        $countiesList  = ['' => 'Select county'];
        $countiesTaxes = [];

        foreach ($Countries as $Country) {

            $States = State::orderBy('state_name')->where('country_id', $Country->id)->get();

            if (count($States) > 0) {

                foreach ($States as $State) {

                    $Counties = County::orderBy('county_name')->where('state_id', $State->id)->get();

                    if (count($Counties) > 0) {

                        $optGroup = $Country->country_name.' -> '.$State->state_name;

                        foreach ($Counties as $County) {
                            $countiesList[$optGroup][$County->id] = $County->county_name;
                            $countiesTaxes[$County->id]           = $County->tax_rate;
                        }
                    }
                }

            }
        }

        return view('taxes.edit')->with('Tax', $Tax)->with('countiesList', $countiesList)->with('countiesTaxes', json_encode($countiesTaxes));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'income_value' => ['required', 'numeric', 'min:0', 'not_in:0', 'regex:/^(\d{1,2}|\d{3,4}|\d{5,6})(\.\d{1,2})?$/'],
            'county_id'    => 'required|regex:/^[0-9]*$/',
        ]);

        $Tax = Tax::find($id);

        if ($Tax === null) {
            return redirect('/taxes')->with('error', 'Invalid tax ID');
        }

        $County = County::find($request->input('county_id'));

        if ($County === null) {
            return redirect('/taxes')->with('error', 'Invalid county ID');
        }

        $Tax->income_value = $request->input('income_value');
        $Tax->tax_rate     = $County->tax_rate;
        $Tax->tax_amount   = round($request->input('income_value') * $County->tax_rate / 100, 2);
        $Tax->county_id    = $request->input('county_id');
        $Tax->save();

        return redirect('/taxes')->with('success', 'Tax updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $Tax = Tax::find($id);

        if ($Tax === null) {
            return redirect('/taxes')->with('error', 'Invalid tax ID');
        }

        $Tax->delete();

        return redirect('/taxes')->with('success', 'Tax deleted');
    }
}
