<?php

namespace App\Http\Controllers;

use App\Country;
use App\County;
use App\State;
use App\Tax;
use Illuminate\Http\Request;

class CountiesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $Counties = County::orderBy('id')->paginate(10);

        return view('counties.index')->with('Counties', $Counties);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $Countries  = Country::orderBy('country_name')->get();
        $statesList = ['' => 'Select state'];

        foreach ($Countries as $Country) {

            $States = State::orderBy('state_name')->where('country_id', $Country->id)->get();

            if (count($States) > 0) {
                $statesList[$Country->country_name] = [];

                foreach ($States as $State) {
                    $statesList[$Country->country_name][$State->id] = $State->state_name;
                }
            }
        }

        return view('counties.create')->with('statesList', $statesList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'county_name' => 'required|unique:counties,county_name,NULL,id,state_id,'.$request->input('state_id'),
            'state_id'    => 'required|regex:/^[0-9]*$/',
            'tax_rate'    => 'required|regex:/^\d{1,2}(\.\d{1,2})?$/',
        ]);

        $State = State::find($request->input('state_id'));

        if ($State === null) {
            return redirect('/counties')->with('error', 'Invalid state ID');
        }

        $County              = new County();
        $County->county_name = $request->input('county_name');
        $County->state_id    = $request->input('state_id');
        $County->tax_rate    = $request->input('tax_rate');
        $County->save();

        return redirect('/counties')->with('success', 'County added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $County = County::find($id);

        if ($County === null) {
            return redirect('/counties')->with('error', 'Invalid county ID');
        }

        $Countries  = Country::orderBy('country_name')->get();
        $statesList = ['' => 'Select state'];

        foreach ($Countries as $Country) {

            $States = State::orderBy('state_name')->where('country_id', $Country->id)->get();

            if (count($States) > 0) {
                $statesList[$Country->country_name] = [];

                foreach ($States as $State) {
                    $statesList[$Country->country_name][$State->id] = $State->state_name;
                }
            }
        }

        return view('counties.edit')->with('County', $County)->with('statesList', $statesList);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'county_name' => 'required|unique:counties,county_name,'.$id.',id,state_id,'.$request->input('state_id'),
            'state_id'    => 'required|regex:/^[0-9]*$/',
            'tax_rate'    => 'required|regex:/^\d{1,2}(\.\d{1,2})?$/',
        ]);

        $County = County::find($id);

        if ($County === null) {
            return redirect('/counties')->with('error', 'Invalid county ID');
        }

        $State = State::find($request->input('state_id'));

        if ($State === null) {
            return redirect('/counties')->with('error', 'Invalid state ID');
        }

        $County->county_name = $request->input('county_name');
        $County->state_id    = $request->input('state_id');
        $County->tax_rate    = $request->input('tax_rate');
        $County->save();

        return redirect('/counties')->with('success', 'County updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $County = County::find($id);

        if ($County === null) {
            return redirect('/counties')->with('error', 'Invalid county ID');
        }

        $Taxes = Tax::where('county_id', $id)->limit(1)->get();

        if (count($Taxes) > 0) {
            return redirect('/counties')->with('error', 'Please remove all taxes from this county');
        }

        $County->delete();

        return redirect('/counties')->with('success', 'County deleted');
    }
}
