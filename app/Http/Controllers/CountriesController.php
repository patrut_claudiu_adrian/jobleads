<?php

namespace App\Http\Controllers;

use App\Country;
use App\State;
use Illuminate\Http\Request;

class CountriesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $Countries = Country::orderBy('id')->paginate(10);

        return view('countries.index')->with('Countries', $Countries);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'country_name' => 'required|max:50|unique:countries',
        ]);

        $Country               = new Country();
        $Country->country_name = $request->input('country_name');
        $Country->save();

        return redirect('/countries')->with('success', 'Country added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $Country = Country::find($id);

        if ($Country === null) {
            return redirect('/countries')->with('error', 'Invalid country ID');
        }

        return view('countries.edit')->with('Country', $Country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'country_name' => 'required|max:50|unique:countries,country_name,'.$id.',id',
        ]);

        $Country = Country::find($id);

        if ($Country === null) {
            return redirect('/countries')->with('error', 'Invalid country ID');
        }

        $Country->country_name = $request->input('country_name');
        $Country->save();

        return redirect('/countries')->with('success', 'Country updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy($id)
    {
        $Country = Country::find($id);

        if ($Country === null) {
            return redirect('/countries')->with('error', 'Invalid country ID');
        }

        $States = State::where('country_id', $id)->limit(1)->get();

        if (count($States) > 0) {
            return redirect('/countries')->with('error', 'Please remove all states from this country');
        }

        $Country->delete();

        return redirect('/countries')->with('success', 'Country deleted');
    }
}
