<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tax
 *
 * @property-read County                     $county
 * @method static \Illuminate\Database\Eloquent\Builder|Tax newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tax newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tax query()
 * @mixin \Eloquent
 * @property int                             $id
 * @property float                           $income_value
 * @property float                           $tax_rate
 * @property float                           $tax_amount
 * @property int                             $county_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereCountyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereIncomeValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereTaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereTaxRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tax whereUpdatedAt($value)
 */
class Tax extends Model
{

    /** @var string */
    protected $table = 'taxes';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function county(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(County::class);
    }
}
