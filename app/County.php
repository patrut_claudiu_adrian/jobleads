<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\County
 *
 * @property-read State                      $state
 * @method static \Illuminate\Database\Eloquent\Builder|County newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|County newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|County query()
 * @mixin \Eloquent
 * @property int                             $id
 * @property string                          $county_name
 * @property int                             $state_id
 * @property float                           $tax_rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|County whereCountyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|County whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|County whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|County whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|County whereTaxRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|County whereUpdatedAt($value)
 */
class County extends Model
{

    /** @var string */
    protected $table = 'counties';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(State::class);
    }
}
