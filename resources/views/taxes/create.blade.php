<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/13/2019
 * Time: 23:00
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Collect Tax</h1>
    {!! Form::open(['action' => 'TaxesController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'Income value')}}
        {{Form::text('income_value', '', ['class' => 'form-control', 'placeholder' => 'Income value'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'County')}}
        {{Form::select('county_id', $countiesList, null, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax rate [%]')}}
        {{Form::text('tax_rate', '', ['class' => 'form-control', 'placeholder' => 'Tax rate', 'readonly' => 'readonly'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax amount')}}
        {{Form::text('tax_amount', '', ['class' => 'form-control', 'placeholder' => 'Tax amount', 'readonly' => 'readonly'])}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    <script>
        var countyTaxes = {!! $countiesTaxes !!};
    </script>
@endsection
