<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/15/2019
 * Time: 21:24
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Edit Tax</h1>
    {!! Form::open(['action' => ['TaxesController@update', $Tax->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'Income value')}}
        {{Form::text('income_value', $Tax->income_value, ['class' => 'form-control', 'placeholder' => 'Income value'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'County')}}
        {{Form::select('county_id', $countiesList, $Tax->county_id, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax rate [%]')}}
        {{Form::text('tax_rate', $Tax->tax_rate, ['class' => 'form-control', 'placeholder' => 'Tax rate', 'readonly' => 'readonly'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax amount')}}
        {{Form::text('tax_amount', $Tax->tax_amount, ['class' => 'form-control', 'placeholder' => 'Tax amount', 'readonly' => 'readonly'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    <script>
        var countyTaxes = {!! $countiesTaxes !!};
    </script>
@endsection
