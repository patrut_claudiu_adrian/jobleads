<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 12:26
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Taxes</h1>
    <a href="/taxes/create" class="btn btn-primary">Collect tax</a>
    <br><br>
    @if (count($Taxes) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>County</th>
                <th>State</th>
                <th>Country</th>
                <th>Income</th>
                <th>Tax rate</th>
                <th>Tax amount</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Taxes as $Tax)
                <tr>
                    <td>{{$Tax->id}}</td>
                    <td>{{$Tax->county->county_name}}</td>
                    <td>{{$Tax->county->state->state_name}}</td>
                    <td>{{$Tax->county->state->country->country_name}}</td>
                    <td>{{$Tax->income_value}}</td>
                    <td>{{sprintf('%05.2f', $Tax->tax_rate)}}%</td>
                    <td>{{$Tax->tax_amount}}</td>
                    <td>{{$Tax->created_at}}</td>
                    <td>{{$Tax->updated_at}}</td>
                    <td><a href="/taxes/{{$Tax->id}}/edit" class="btn btn-primary">Edit</a></td>
                    <td>
                        {!! Form::open(['action' => ['TaxesController@destroy', $Tax->id], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$Taxes->links()}}
    @else
        <p>No taxes collected</p>
    @endif
@endsection
