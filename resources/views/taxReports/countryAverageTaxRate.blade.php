<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 20:11
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Average tax rate of the country</h1>
    @if (count($Countries) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Country</th>
                <th>Tax rate</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Countries as $Country)
                <tr>
                    <td>{{$Country->country_name}}</td>
                    <td>{{sprintf('%05.2f', $Country->tax_rate)}}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$Countries->links()}}
    @else
        <p>No countries found</p>
    @endif
@endsection
