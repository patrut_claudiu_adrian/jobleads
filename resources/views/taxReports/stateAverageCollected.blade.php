<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 20:11
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Average amount of taxes collected per state</h1>
    @if (count($Taxes) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Country</th>
                <th>State</th>
                <th>Tax amount</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Taxes as $Tax)
                <tr>
                    <td>{{$Tax->country_name}}</td>
                    <td>{{$Tax->state_name}}</td>
                    <td>{{$Tax->tax_amount}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$Taxes->links()}}
    @else
        <p>No taxes collected</p>
    @endif
@endsection
