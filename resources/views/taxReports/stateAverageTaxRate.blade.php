<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 20:11
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Average county tax rate per state</h1>
    @if (count($States) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Country</th>
                <th>State</th>
                <th>Tax rate</th>
            </tr>
            </thead>
            <tbody>
            @foreach($States as $State)
                <tr>
                    <td>{{$State->country_name}}</td>
                    <td>{{$State->state_name}}</td>
                    <td>{{sprintf('%05.2f', $State->tax_rate)}}%</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$States->links()}}
    @else
        <p>No states found</p>
    @endif
@endsection
