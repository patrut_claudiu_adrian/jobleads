<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 12:26
 */
?>
@extends('layouts.app')
@section('content')
    <h1>States</h1>
    <a href="/states/create" class="btn btn-primary">Add state</a>
    <br><br>
    @if (count($States) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>State id</th>
                <th>State name</th>
                <th>Country name</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($States as $State)
                <tr>
                    <td>{{$State->id}}</td>
                    <td>{{$State->state_name}}</td>
                    <td>{{$State->country->country_name}}</td>
                    <td>{{$State->created_at}}</td>
                    <td>{{$State->updated_at}}</td>
                    <td><a href="/states/{{$State->id}}/edit" class="btn btn-primary">Edit</a></td>
                    <td>
                        {!! Form::open(['action' => ['StatesController@destroy', $State->id], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$States->links()}}
    @else
        <p>No states found</p>
    @endif
@endsection
