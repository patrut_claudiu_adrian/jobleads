<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/13/2019
 * Time: 23:00
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Add State</h1>
    {!! Form::open(['action' => 'StatesController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'State name')}}
        {{Form::text('state_name', '', ['class' => 'form-control', 'placeholder' => 'State name'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Country')}}
        {{Form::select('country_id', $countriesList, null, ['class' => 'form-control'])}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
