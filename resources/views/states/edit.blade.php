<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/15/2019
 * Time: 21:24
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Edit State</h1>
    {!! Form::open(['action' => ['StatesController@update', $State->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'State name')}}
        {{Form::text('state_name', $State->state_name, ['class' => 'form-control', 'placeholder' => 'State name'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Country')}}
        {{Form::select('country_id', $countriesList, $State->country_id, ['class' => 'form-control'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
