@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">{{$User->name}} - Dashboard</div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Total number</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Countries</td>
                            <td>{{$Countries->total_countries}}</td>
                            <td><a href="/countries" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/countries/create" class="btn btn-success btn-sm">Add</a></td>
                        </tr>
                        <tr>
                            <td>States</td>
                            <td>{{$States->total_states}}</td>
                            <td><a href="/states" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/states/create" class="btn btn-success btn-sm">Add</a></td>
                        </tr>
                        <tr>
                            <td>Counties</td>
                            <td>{{$Counties->total_counties}}</td>
                            <td><a href="/counties" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/counties/create" class="btn btn-success btn-sm">Add</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Value</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Taxes collected</td>
                            <td>{{$TaxesNumber->total_taxes}}</td>
                            <td><a href="/taxes" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/taxes/create" class="btn btn-success btn-sm">Collect</a></td>
                        </tr>
                        <tr>
                            <td>Taxes amount</td>
                            <td>{{$TaxesAmount->taxes_amount}}</td>
                            <td><a href="/taxes" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/taxes/create" class="btn btn-success btn-sm">Collect</a></td>
                        </tr>
                        <tr>
                            <td>Average tax</td>
                            <td>{{$TaxesAverage->taxes_average}}</td>
                            <td><a href="/taxes" class="btn btn-primary btn-sm">View</a></td>
                            <td><a href="/taxes/create" class="btn btn-success btn-sm">Collect</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
