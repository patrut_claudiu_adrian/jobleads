<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 12:26
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Countries</h1>
    <a href="/countries/create" class="btn btn-primary">Add country</a>
    <br><br>
    @if (count($Countries) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Country id</th>
                <th>Country name</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Countries as $Country)
                <tr>
                    <td>{{$Country->id}}</td>
                    <td>{{$Country->country_name}}</td>
                    <td>{{$Country->created_at}}</td>
                    <td>{{$Country->updated_at}}</td>
                    <td><a href="/countries/{{$Country->id}}/edit" class="btn btn-primary">Edit</a></td>
                    <td>
                        {!! Form::open(['action' => ['CountriesController@destroy', $Country->id], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$Countries->links()}}
    @else
        <p>No countries found</p>
    @endif
@endsection
