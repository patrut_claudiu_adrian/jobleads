<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/13/2019
 * Time: 23:00
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Add Country</h1>
    {!! Form::open(['action' => 'CountriesController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'Country name')}}
        {{Form::text('country_name', '', ['class' => 'form-control', 'placeholder' => 'Country name'])}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
