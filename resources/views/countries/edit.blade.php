<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/15/2019
 * Time: 21:24
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Edit Country</h1>
    {!! Form::open(['action' => ['CountriesController@update', $Country->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'Country name')}}
        {{Form::text('country_name', $Country->country_name, ['class' => 'form-control', 'placeholder' => 'Country name'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
