<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/15/2019
 * Time: 21:24
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Edit County</h1>
    {!! Form::open(['action' => ['CountiesController@update', $County->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'County name')}}
        {{Form::text('county_name', $County->county_name, ['class' => 'form-control', 'placeholder' => 'County name'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'State')}}
        {{Form::select('state_id', $statesList, $County->state_id, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax rate [%]')}}
        {{Form::text('tax_rate', $County->tax_rate, ['class' => 'form-control', 'placeholder' => 'Tax rate'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
