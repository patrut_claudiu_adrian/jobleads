<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 12:26
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Counties</h1>
    <a href="/counties/create" class="btn btn-primary">Add county</a>
    <br><br>
    @if (count($Counties) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>County id</th>
                <th>County name</th>
                <th>State name</th>
                <th>Country name</th>
                <th>Tax rate</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Counties as $County)
                <tr>
                    <td>{{$County->id}}</td>
                    <td>{{$County->county_name}}</td>
                    <td>{{$County->state->state_name}}</td>
                    <td>{{$County->state->country->country_name}}</td>
                    <td>{{sprintf('%05.2f', $County->tax_rate)}}%</td>
                    <td>{{$County->created_at}}</td>
                    <td>{{$County->updated_at}}</td>
                    <td><a href="/counties/{{$County->id}}/edit" class="btn btn-primary">Edit</a></td>
                    <td>
                        {!! Form::open(['action' => ['CountiesController@destroy', $County->id], 'method' => 'POST']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$Counties->links()}}
    @else
        <p>No counties found</p>
    @endif
@endsection
