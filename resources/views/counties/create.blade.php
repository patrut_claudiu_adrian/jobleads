<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/13/2019
 * Time: 23:00
 */
?>
@extends('layouts.app')
@section('content')
    <h1>Add County</h1>
    {!! Form::open(['action' => 'CountiesController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'County name')}}
        {{Form::text('county_name', '', ['class' => 'form-control', 'placeholder' => 'County name'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'State')}}
        {{Form::select('state_id', $statesList, null, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{Form::label('title', 'Tax rate [%]')}}
        {{Form::text('tax_rate', '', ['class' => 'form-control', 'placeholder' => 'Tax rate'])}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
