<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/12/2019
 * Time: 12:59
 */
?>
@extends('layouts.app')
@section('content')
    <div class="jumbotron text-center">
        <h1>{{$title}}</h1>
        <p>This is an application for hiring purpose</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> <a class="btn btn-success btn-lg" href="/register" role="button">Register</a></p>
    </div>
@endsection
