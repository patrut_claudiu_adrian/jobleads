$(document).ready(function(){
    $('[name="county_id"]').change(function(){

        if ($(this).val() !== '') {
            $('[name="tax_rate"]').val(countyTaxes[$(this).val()]);
        } else {
            $('[name="tax_rate"]').val('');
        }

        updateTaxAmount();
    });

    $('[name="income_value"]').keyup(function(){
        updateTaxAmount();
    });

    updateTaxAmount();
});

function updateTaxAmount(){
    if ($('[name="tax_rate"]').val() !== '' && $('[name="income_value"]').val() !== ''){
        var taxAmount = $('[name="income_value"]').val() * $('[name="tax_rate"]').val() / 100;
        $('[name="tax_amount"]').val(taxAmount.toFixed(2));
    } else {
        $('[name="tax_amount"]').val('');
    }
}
