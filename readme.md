# Jobleads

### Requirements
1. Apache 2.4
2. PHP 7.2
3. MariaDB 10
4. Composer

### Installation

1. Setup a virtual host in apache and point it to public folder
2. Copy file .env.example to .env
3. Fill in database credentials ( DB_* )
4. Run in terminal
```sh
$ composer install
$ php artisan migrate
$ php artisan key:generate
```
