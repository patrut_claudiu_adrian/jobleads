<?php
/**
 * Created by PhpStorm.
 * User: CPA
 * Date: 5/18/2019
 * Time: 10:51
 */

/**
 * Class StringTools
 * Provides a set of methods to handle strings
 */
class StringTools
{

    public const HASH_ALGORITHMS
        = [
            'md2'        => 'md2',
            'md4'        => 'md4',
            'md5'        => 'md5',
            'sha1'       => 'sha1',
            'sha256'     => 'sha256',
            'sha384'     => 'sha384',
            'sha512'     => 'sha512',
            'ripemd128'  => 'ripemd128',
            'ripemd160'  => 'ripemd160',
            'ripemd256'  => 'ripemd256',
            'ripemd320'  => 'ripemd320',
            'whirlpool'  => 'whirlpool',
            'tiger128,3' => 'tiger128,3',
            'tiger160,3' => 'tiger160,3',
            'tiger192,3' => 'tiger192,3',
            'tiger128,4' => 'tiger128,4',
            'tiger160,4' => 'tiger160,4',
            'tiger192,4' => 'tiger192,4',
            'snefru'     => 'snefru',
            'gost'       => 'gost',
            'adler32'    => 'adler32',
            'crc32'      => 'crc32',
            'crc32b'     => 'crc32b',
            'haval128,3' => 'haval128,3',
            'haval160,3' => 'haval160,3',
            'haval192,3' => 'haval192,3',
            'haval224,3' => 'haval224,3',
            'haval256,3' => 'haval256,3',
            'haval128,4' => 'haval128,4',
            'haval160,4' => 'haval160,4',
            'haval192,4' => 'haval192,4',
            'haval224,4' => 'haval224,4',
            'haval256,4' => 'haval256,4',
            'haval128,5' => 'haval128,5',
            'haval160,5' => 'haval160,5',
            'haval192,5' => 'haval192,5',
            'haval224,5' => 'haval224,5',
            'haval256,5' => 'haval256,5',
        ];

    /**
     * Concatenates two strings
     *
     * @param string $input1
     * @param string $input2
     *
     * @return string
     */
    public static function concat($input1, $input2): string
    {
        return self::concatenate($input1, $input2);
    }

    /**
     * Writes a new line at end of string
     *
     * @param string $input
     *
     * @return string
     */
    public static function writeLn($input): string
    {
        return $input.PHP_EOL;
    }

    /**
     * Makes $input to upper case
     *
     * @param string $input
     *
     * @return string
     */
    public static function toUpperCase($input): string
    {
        return strtoupper($input);
    }

    /**
     * Makes $input to lower case
     *
     * @param string $input
     *
     * @return string
     */
    public static function toLowerCase($input): string
    {
        return strtolower($input);
    }

    /**
     * Calculates hash for $input using provided $algorithm
     *
     * @param string $algorithm
     * @param string $input
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    public static function hash($algorithm, $input): string
    {
        if ( ! in_array(strtolower($algorithm), self::HASH_ALGORITHMS, true)) {
            throw new \RuntimeException('Invalid hash algorithm provided');
        }

        return hash($algorithm, $input);
    }

    /**
     * Calculates hash for $input using md5 algorithm
     *
     * @param string $input
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    public static function md5($input): string
    {
        return self::hash(self::HASH_ALGORITHMS['md5'], $input);
    }


    /**
     * Calculates hash for $input using sha512 algorithm
     *
     * @param string $input
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    public static function sha512($input): string
    {
        return self::hash(self::HASH_ALGORITHMS['sha512'], $input);
    }

    /**
     * Concatenates all strings received as parameters in ...$inputs
     *
     * @param string ...$inputs
     *
     * @return string
     */
    public static function concatenate(...$inputs): string
    {
        return implode('', $inputs);
    }


    /**
     * Replaces all occurrences of string $needle with string $replace in string $haystack
     *
     * @param string $needle   Needle to search for
     * @param string $replace  Replace - value to replace the needle
     * @param string $haystack Haystack - string to search the needle in
     *
     * @return string
     */
    public static function replace($needle, $replace, $haystack): string
    {
        return str_replace($needle, $replace, $haystack);
    }

    /**
     * Trims string of characters from beginning and end " ", "\t", "\n", "\r", "\0", "\x0B"
     *
     * @param $input string
     *
     * @return string
     */
    public static function trim($input): string
    {
        return trim($input);
    }
}
